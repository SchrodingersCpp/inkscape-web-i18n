This repository is the language directory for inkscape-web.

Once a week, the translations are synced with the branch of inkscape-web that 
is available on the live website.

## Translation

To translate the strings for inkscape-web, you can download the files from
[here](https://gitlab.com/inkscape/inkscape-web-i18n/repository/archive.zip?ref=master)
or use git to clone the repository.

After unpacking / git cloning, go to the directory for your language, and edit
the file `django.po` that is inside, preferably with a specialized translation
software, to get the formatting right.

Make sure to add your author credits to the file, like it is done [here (for French)](https://gitlab.com/inkscape/inkscape-web-i18n/blob/master/fr/LC_MESSAGES/django.po).

If you're a long-time contributor, you may want to check the dates of your 
contributions (once a year).

To get your changes up into this repository, please either attach your django.po file
to [a new issue](https://gitlab.com/inkscape/inkscape-web-i18n/issues) , or
push to the repository. You can also make a merge request, if you'd like a review,
or if you're helping here for the first time.
If you need permissions to push to the master branch,
please let us know (via issue) so they can be adjusted.

If you would like to get a message when the files have been updated (in some weeks, 
there is nothing to update, and so nothing new to translate), please send an email
to the translators' mailing list, from the email address where the message should be 
sent.

Pushing to the "live" branch is restricted.

## Updating the repository to match the web server


Within an installation of inkscape-web, it's located in and should be installed to ~websitedir/data/locale/website

To update the po files to match live, run:

`./utils/manage makemessages --configured-list --no-obsolete`

in the live branch of inkscape-web that is at the same revision as the website.

You should test if messages compile once you have the po files themselves:
`./utils/manage compilemessages`

